import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'ActivityHome',
    component: () => import('./views/ActivityHome.vue')
  },
  {
    path: '/vote',
    name: 'Vote',
    component: () => import('./views/Vote.vue')
  },
  {
    path: '/invalid',
    name: 'Invalid',
    component: () => import('./views/Invalid.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('./views/Home.vue')
  },
  {
    path: '/activity',
    name: 'ActivityCreate',
    component: () => import('./views/ActivityCreate.vue')
  },

  // {
  //   path: '/irene_console',
  //   name: 'Console',
  //   component: () => import('./views/Console.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
