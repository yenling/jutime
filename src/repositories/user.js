export const UserRepo = {
  responseId: {
    get: (actId) => {
      return new Promise((res) => {
        let resId = localStorage.getItem('jutime-responseId-' + actId)
        if (resId) {
          res(resId)
        }
        else {
          res(null)
        }
      })
    },

    post: (actId, resId) => {
      return new Promise((res) => {
        localStorage.setItem('jutime-responseId-' + actId, resId)
        res()
      })
    }
  }
}
