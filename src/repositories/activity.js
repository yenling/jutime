import { collection, doc, getDoc, getDocs, addDoc, setDoc, onSnapshot } from "firebase/firestore"
import db from "../connection/firebase"

export const ActivityRepo = {
  activity: {
    get: (actId) => {
      return new Promise(async (res, rej) => {
        const docSnap = await getDoc(doc(db, "activities", actId))

        if (docSnap.exists()) {
          res(docSnap.data())
        } else {
          rej()
        }
      })
    },
    post: async (data) => {
      const docRef = await addDoc(collection(db, 'activities'), data)
      return docRef.id
    }
  },

  responses: {
    get: (actId) => {
      return new Promise(async (res) => {
        const docsSnap = await getDocs(collection(db, "activities", actId, "responses"))
        let responses = []
        docsSnap.forEach(doc => {
          responses.push(doc.data())
        })
        res(responses)
      }
      )
    },

    listen: (actId, callback) => {
      onSnapshot(collection(db, "activities", actId, 'responses'), (_coll) => {
        let records = []
        _coll.forEach((_doc) => {
          records.push(_doc.data())
        })
        callback(records)
      })

    }
  },

  response: {
    get: (actId, resId) => {
      return new Promise(async (res, rej) => {
        try {
          const docSnap = await getDoc(doc(db, "activities", actId, "responses", resId))
          res(docSnap.data())
        }
        catch (e) {
          console.warn(e)
          rej(e)
        }
      })
    },

    put: (actId, resId, data) => {
      return new Promise(async (res) => {
        if (resId) {
          // update
          const docRef = await setDoc(doc(db, "activities", actId, "responses", resId), data)
          res(resId)
        }
        else {
          // create
          const docRef = await addDoc(collection(db, "activities", actId, "responses"), data)
          res(docRef.id)
        }
      })
    }
  }
}
