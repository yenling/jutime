import { reactive, readonly, provide } from 'vue'
import { ActivityRepo } from '../../repositories/activity'

import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
dayjs.extend(duration)

export const useActivityStore = () => {
  const state = reactive({
    actInfo: null,
    actResponses: [],
    restTime: undefined,
  })

  const fetchActivityInfo = (actId) => {
    return new Promise((res, rej) => {
      if (actId) {
        return ActivityRepo.activity.get(actId)
          .then((_actInfo) => {
            _actInfo.deadline = _actInfo.deadline?.seconds ? new Date(_actInfo.deadline.seconds * 1000) : null
            state.actInfo = _actInfo
            state.actId = actId
            state.restTime = _actInfo.deadline ? _actInfo.deadline.getTime() - new Date().getTime() : null
            fetchResponses(actId)
            res()
          })
          .catch((e) => {
            rej(e)
          })
      }
      else {
        rej()
      }
    })
  }

  const createActivity = (data) => {
    return ActivityRepo.activity.post(data)
  }

  const fetchResponses = (actId) => {
    return new Promise((res) => {
      if (actId) {
        return ActivityRepo.responses.get(actId)
          .then((_responses) => {
            state.actResponses = _responses
            listenResponses(actId)
            res()
          })
      }
      else {
        rej()
      }
    })
  }

  const listenResponses = (actId) => {
    ActivityRepo.responses.listen(actId, (records) => {
      state.actResponses = records
    })
  }

  const store = {
    state: readonly(state),
    fetchActivityInfo,
    createActivity
  }

  provide('ActivityStore', store)

  return store
}
