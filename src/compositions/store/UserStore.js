import { reactive, readonly, provide } from 'vue'
import { UserRepo } from '../../repositories/user'
import { ActivityRepo } from '../../repositories/activity'

export const useUserStore = () => {
  const state = reactive({
    resId: null,
    response: null
  })

  const getResponseId = (actId) => {
    return UserRepo.responseId.get(actId)
      .then((resId) => {
        state.resId = resId
        return resId
      })
  }

  const fetchResponse = (actId, resId) => {
    return ActivityRepo.response.get(actId, resId)
      .then((response) => {
        state.response = response
      })
  }

  const saveResponseId = (actId, resId) => {
    return UserRepo.responseId.post(actId, resId)
      .then(() => {
        state.resId = resId
      })
  }

  const submitResponse = (actId, resId, data) => {
    return ActivityRepo.response.put(actId, resId, data)
      .then(async (resId) => {
        await saveResponseId(actId, resId)
        state.response = data
      })
  }

  const store = {
    state: readonly(state),
    getResponseId,
    saveResponseId,
    submitResponse,
    fetchResponse
  }

  provide('UserStore', store)

  return store
}
