## 展示頁
https://jutime-53595.web.app/?actId=rNnfXPxYS9wpToewkV4q

## 說明
一個統計聚會日期的工具網頁。
連結為展示用活動的投票主頁，可以瀏覽投票狀況、投票、編輯投票紀錄。
在網頁根目錄 (https://jutime-53595.web.app/) 有簡單的新增活動功能，預計還會再加上使用者驗證步驟。
此專案使用了 Firebase 提供的資料庫、部署服務。
